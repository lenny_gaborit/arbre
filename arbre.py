from graphviz import Digraph

class Noeud:
    """
    Un noeud a une valeur et pointe vers deux autres noeuds (petit et
    grand) ou éventuellement le vide.
    On insère de nouvelles valeurs en partant de la racine du noeud et
    en bifurquant selon la comparaison avec la valeur du noeud.

    """

    def __init__(self, val):
        """
        Un noeud a toujours une valeur mais pointe vers un autre noeud
        ou éventuellement le vide (None)
        """
        self.__val = val
        self.__grand = None
        self.__petit = None

    def insere(self, val):
        """
        Permet d'insérer une valeur dans un noeud.
        """
        if val < self.__val :
            if self.__petit is None:
                self.__petit = Noeud(val)
            else:
                self.__petit.insere(val)
        elif val > self.__val:
            if self.__grand is None:
                self.__grand = Noeud(val)
            else:
                self.__grand.insere(val)
            
    def hauteur(self) -> int:
        if self.__petit is None:
            return  self.__grand.hauteur()
        elif self.__grand is None:
            return self.__petit.hauteur()
        elif self.__petit is None and self.__grand is None:
            return 1
        else:
            return max(1 + self.__grand.hauteur(),1 + self.__petit.hauteur())

    def nb_noeuds(self) -> int:
        compteur = 0
        if self.__petit is None and self.__grand is None:
            compteur += 1
            return compteur
        elif self.__petit is None:
            compteur +=1 + self.__grand.nb_noeuds()
            return compteur
        elif self.__grand is None:
            compteur += 1 + self.__petit.nb_noeuds()    
            return 1 + self.__petit.nb_noeuds()           
        else:
            compteur += (1 + self.__petit.nb_noeuds()) + self.__grand.nb_noeuds()
            return  compteur 

    def est_feuille(self) -> bool:
        if self.__petit is None and self.__grand is None :
            return True


    def nb_feuilles(self) -> int:
        pass
     
    def contient(self, v) -> bool:
        if self.__val == v:
            return (f"{v}"" est dans l'abre")
        elif self.__val > v :
            return self.__petit.contient(v)
        elif self.__val < v :
            return self.__grand.contient(v)
        else :
            return (f"{v}" " n'est pas dans l'arbre")

    def visite_pre(self) -> None:
        """
        Que fait ce code ?
        Expliquez la différence avec les deux suivants
        """
        """
        cette fonction affiche les valeur de l'abre lorsqu'il passe coter d'une valeur a gauche 
        elle correspond au parcours préfixe
        """
        print(self.__val)
        for cote in [self.__petit, self.__grand]:
            if cote is not None:
                cote.visite_pre()

    def visite_post(self) -> None:
        """
        cette fonction affiche les valeur de l'abre lorsqu'il passe coter d'une valeur a droite 
        elle correspond au parcours postfixe
        """
        for cote in [self.__petit, self.__grand]:
            if cote is not None:
                cote.visite_post()
        print(self.__val)

    def visite_inf(self) -> None:
        """
        cette fonction affiche les valeur de l'abre lorsqu'il passe en dessous d'une valeur 
        elle correspond au parcours infixe
        """
        if self.__petit is not None:
            self.__petit.visite_inf()
        print(self.__val)
        if self.__grand is not None:
            self.__grand.visite_inf()

    def visite_niveau(self) -> None:
        """
        On veut obtenir les noeuds par niveau i.e. classés dans l'ordre 
        croissant des distances à la racine aka parcours en largeur 
        """
        pass

    def mini(self):
        """
        La plus petite valeur 
        """
        pass
        
    def maxi(self):
        """
        la plus grande valeur
        """
        pass
        
    #  Outils de représentation
    
    def viz(self):
        s = Digraph()
        def tree2viz(self):
            """
            À compléter...
            """
            v = self.__val
            if self.__petit and self.__grand:
                s.node(f'{self.__petit.__val}', style='filled',fillcolor='cadetblue3') #crée un noeud graphique avec self.__petit
                s.node(f'{self.grand.__val}', style='filled',fillcolor='cadetblue3') #crée un noeud graphique avec self.__grand
                s.edge(f'{self}', f'nullg{self.__petit}') #crée le lien entre self et self.__petit
                s.edge(f'{self}', f'nullg{self.__grand}') #crée le lien entre self et self.__grand
                tree2viz(self.__petit)
                tree2viz(self.__grand)
            elif self.__petit and not self.__grand:
                s.node(f'{self.__petit.__val}', style='filled',fillcolor='cadetblue3')
                s.edge(f'{self}', f'nullg{self.__petit}')
                tree2viz(self.__petit)
            elif self.__grand and not self.__petit:
                s.node(f'{self.__grand.__val}', style='filled',fillcolor='cadetblue3')
                s.edge(f'{self}', f'nullg{self.__grand}')
                tree2viz(self.__grand)
            else:
                s.node(f'nullg{v}',shape='point')
                s.edge(f'{v}', f'nullg{v}')
                s.node(f'nullp{v}',shape='point')
                s.edge(f'{v}', f'nullp{v}')
        tree2viz(self)
        return s

    def affiche(self):
        """
        """
        s = self.viz()
        s.graph_attr['ordering']='out'
        return s


N = Noeud(1);N.insere(2);N.insere(4);N.insere(3);N.insere(10);N.insere(6)
N.affiche()

class Arbre:
    """
    Arbre binaire de recherche constitué de noeuds.
    Reprend les méthodes de la classe Noeud en incluant le cas vide
    et en construisant un arbre à partir d'un noeud.
    """

    def __init__(self) -> None:
        """
        Constructeur : un arbre est vide ou constitué de noeuds
        """
        self.__data = None

    def est_vide(self) -> bool:
        """
        Testeur : vérifie si un arbre est vide
        """
        return self.__data is None

    def insere(self, val) -> None:
        """
        Insère un élément comparable dans un arbre selon le critère
        choisi pour les noeuds.
        Si l'arbre est vide, crée le noeud-data
        """
        if self.__data is None:
            self.__data = Noeud(val)
        else:
            self.__data.insere(val)

    def hauteur(self) -> int:
        if self.__data is None:
            return 0
        else:
            return self.__data.hauteur()

    def nb_noeuds(self) -> int:
        if self.__data is None:
            return 0
        else:
            return self.__data.nb_noeuds()

    def est_feuille(self) -> bool:
        if self.__data is None:
            return False
        else:
            return self.__data.est_feuille()

    def nb_feuilles(self) -> int:
        if self.__data is None:
            return 0
        else:
            return self.__data.nb_feuilles()

    def contient(self, v) -> bool:
        if self.__data is None:
            return False
        return self.__data.contient(v)

    def visite_pre(self) -> None:
        if self.__data:
            self.__data.visite_pre()

    def visite_post(self) -> None:
        if self.__data:
            self.__data.visite_post()

    def visite_inf(self) -> None:
        if self.__data:
            self.__data.visite_inf()

    def visite_inf_imp(self) -> None:
        if self.__data:
            self.__data.visite_inf_imp()

    def visite_niveau(self) -> None:
        if self.__data:
            self.__data.visite_niveau()

    def mini(self):
        assert self.__data, 'Arbre vide ! Pas de minimum'
        return self.__data.mini()

    def maxi(self):
        assert self.__data, 'Arbre vide ! Pas de maximum'
        return self.__data.maxi()

    def affiche(self) -> None:
        assert self.__data, 'Arbre vide'
        return self.__data.affiche()

    def __str__(self) -> str:
        if self.__data is None:
            return 'Arbre_Vide'
        else:
            return self.__data.__str__()






